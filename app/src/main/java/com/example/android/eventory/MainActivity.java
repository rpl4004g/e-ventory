package com.example.android.eventory;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {

    ImageButton btnDashboard, btnMaterial, btnMaterial_in, btnMaterial_out, btnFinancial, btnWarehouse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnMaterial = findViewById(R.id.btn_material);
        btnMaterial_in = findViewById(R.id.btn_material_in);
        btnMaterial_out = findViewById(R.id.btn_material_out);
        btnFinancial = findViewById(R.id.btn_financial);
        btnWarehouse = findViewById(R.id.btn_warehouse);

        btnMaterial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, MaterialAdd.class));
            }
        });
        btnMaterial_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, inventoryin.class));
            }
        });
        btnMaterial_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, inventoryout.class));
            }
        });
        btnFinancial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, financial.class));
            }
        });
        btnWarehouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, warehouse.class));
            }
        });
        btnFinancial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, financial.class));
            }
        });
    }
}
